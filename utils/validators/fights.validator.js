const errorMessages = require("../../constants/error-messages");
const { fight: fightModel } = require("../../models/fight");
const FighterService = require("../../services/fighterService");

function fightCreateValidator(fight) {
  if ("id" in fight) {
    return errorMessages.SHOULD_NOT_CONTAIN_ID;
  }

  if (!"fighter1" in fight) {
    return errorMessages.INVALID_EMAIL;
  }

  if (!"fighter2" in fight) {
    return errorMessages.INVALID_PHONE_NUMBER;
  }

  // Check is exists fighter1 & fighter2 in fighters db
  if (!isFighterExist(fight.fighter1)) {
    return `${errorMessages.FIGHTER_NOT_EXISTS} \n fighter id ${fight.fighter1} doesnt exist`;
  }
  if (!isFighterExist(fight.fighter2)) {
    return `${errorMessages.FIGHTER_NOT_EXISTS} \n fighter id ${fight.fighter2} doesnt exist`;
  }
  if (!(fight.log instanceof Array) || fight.log.length == 0) {
    return errorMessages.FIGHT_LOG_EMPTY;
  }
  for (let log in fight.log) {
    if (!(log instanceof Object)) {
      return errorMessages.INVALID_FIGHT_LOG;
    }
    if (
      !("fighter1Shot" in log) ||
      !("fighter2Shot" in log) ||
      !("fighter1Health" in log) ||
      !("fighter2Health" in log)
    ) {
      return errorMessages.INVALID_FIGHT_LOG;
    }
    //convert to numbers if its Nan then previous value is undefined/null
    if (
      isNaN(+log.fighter1Shot) ||
      isNaN(+log.fighter2Shot) ||
      isNaN(+log.fighter1Health) ||
      isNaN(+log.fighter2Health)
    ) {
      return errorMessages.INVALID_FIGHT_LOG;
    }
  }

  for (let key in fight) {
    if (!(key in fightModel)) {
      return `${errorMessages.HAS_EXTRA_FIELDS} \n unknown property "${key}"`;
    }
  }

  return null;
}

function isFighterExist(fighterId) {
  return FighterService.isExist(fighterId);
}
module.exports = fightCreateValidator;

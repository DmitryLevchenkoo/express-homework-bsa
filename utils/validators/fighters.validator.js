const errorMessages = require("../../constants/error-messages");
const { fighter: fighterModel } = require("../../models/fighter");

function fighterCreateValidator(fighter) {
  // convert json "string" format in Number
  fighter.health = +fighter.health || 100;
  fighter.defense = +fighter.defense;
  fighter.power = +fighter.power;

  if ("id" in fighter) {
    return errorMessages.SHOULD_NOT_CONTAIN_ID;
  }

  if (!isNameValid(fighter.name)) {
    return errorMessages.INVALID_FIGHTER_NAME;
  }

  if (!isDefenseValid(fighter.defense)) {
    return errorMessages.INVALID_DEFENCE;
  }
  if (!isPowerValid(fighter.power)) {
    return errorMessages.INVALID_POWER;
  }
  if (!isHealthValid(fighter.health)) {
    return errorMessages.INVALID_HEALTH;
  }

  for (let key in fighter) {
    if (!(key in fighterModel)) {
      return errorMessages.HAS_EXTRA_FIELDS;
    }
  }

  return null;
}

function fighterUpdateValidator(fighter) {
  // convert json "string" format in Number
  fighter.defense = +fighter.defense;
  fighter.power = +fighter.power;

  if ("id" in fighter) {
    return errorMessages.SHOULD_NOT_CONTAIN_ID;
  }

  if ("name" in fighter && !isNameValid(fighter.name)) {
    return errorMessages.INVALID_FIGHTER_NAME;
  }

  if ("defense" in fighter && !isDefenseValid(fighter.defense)) {
    return errorMessages.INVALID_DEFENCE;
  }
  if ("power" in fighter && !isPowerValid(fighter.power)) {
    return errorMessages.INVALID_POWER;
  }
  if ("health" in fighter && !isHealthValid(fighter.health)) {
    return errorMessages.INVALID_HEALTH;
  }

  for (let key in fighter) {
    if (!(key in fighterModel)) {
      return errorMessages.HAS_EXTRA_FIELDS;
    }
  }

  return null;
}
function isNameValid(name) {
  return (
    name && typeof name === "string" && name.length < 256 && name.length > 1
  );
}

function isDefenseValid(defense) {
  return !isNaN(defense) && defense >= 1 && defense <= 10;
}

function isPowerValid(power) {
  return !isNaN(power) && power > 1 && power < 100;
}

function isHealthValid(health) {
  return !isNaN(health) && health > 80 && health < 120;
}

module.exports.fighterCreateValidator = fighterCreateValidator;
module.exports.fighterUpdateValidator = fighterUpdateValidator;

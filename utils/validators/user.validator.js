const errorMessages = require("../../constants/error-messages");
const { user: userModel } = require("../../models/user");
function userCreateValidator(user) {
  if ("id" in user) {
    return errorMessages.SHOULD_NOT_CONTAIN_ID;
  }

  if (!isEmailValid(user.email)) {
    return errorMessages.INVALID_EMAIL;
  }

  if (!isPhoneNumberValid(user.phoneNumber)) {
    return errorMessages.INVALID_PHONE_NUMBER;
  }

  if (!isPasswordValid(user.password)) {
    return errorMessages.INVALID_PASSWORD;
  }

  if (!isNameValid(user.firstName)) {
    return errorMessages.INVALID_NAME;
  }

  if (!isNameValid(user.lastName)) {
    return errorMessages.INVALID_LAST_NAME;
  }

  for (let key in user) {
    if (!(key in userModel)) {
      return `${errorMessages.HAS_EXTRA_FIELDS} \n unknown property "${key}"`;
    }
  }

  return null;
}
function userUpdateValidator(user) {
  if ("id" in user) {
    return errorMessages.SHOULD_NOT_CONTAIN_ID;
  }

  if ("email" in user && !isEmailValid(user.email)) {
    return errorMessages.INVALID_EMAIL;
  }

  if ("phone" in user && !isPhoneNumberValid(user.phoneNumber)) {
    return errorMessages.INVALID_PHONE_NUMBER;
  }

  if ("password" in user && !isPasswordValid(user.password)) {
    return errorMessages.INVALID_PASSWORD;
  }

  if ("firstName" in user && !isNameValid(user.firstName)) {
    return errorMessages.INVALID_NAME;
  }

  if ("lastName" in user && !isNameValid(user.lastName)) {
    return errorMessages.INVALID_LAST_NAME;
  }

  for (let key in user) {
    if (!(key in userModel)) {
      return `${errorMessages.HAS_EXTRA_FIELDS} \n unknown property "${key}"`;
    }
  }

  return null;
}

function isEmailValid(email) {
  return (
    email &&
    typeof email === "string" &&
    /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@gmail.com$/.test(email) //only gmail
  );
}

function isPhoneNumberValid(phoneNumber) {
  return (
    phoneNumber &&
    typeof phoneNumber === "string" &&
    /^\+380[0-9]{9}$/.test(phoneNumber) //only +380xxxxxxxxx
  );
}

function isPasswordValid(password) {
  return (
    password &&
    typeof password === "string" &&
    password.length >= 3 &&
    password.length < 200
  );
}

function isNameValid(name) {
  return (
    name && typeof name === "string" && name.length < 200 && name.length > 1
  );
}

module.exports.userCreateValidator = userCreateValidator;
module.exports.userUpdateValidator = userUpdateValidator;

const { UserRepository } = require("../repositories/userRepository");
const errorMessage = require("../constants/error-messages");
class UserService {
  // TODO: Implement methods to work with user

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
  getAll() {
    const items = UserRepository.getAll();
    return items;
  }
  getOne(id) {
    if (!this.isExist(id)) {
      throw {
        statusCode: 404,
        message: errorMessage.USER_NOT_EXISTS,
      };
    }
    return UserRepository.getOne({ id });
  }
  addOne(data) {
    return UserRepository.create(data);
  }
  updateOne(id, dataToUpdate) {
    if (!this.isExist(id)) {
      throw {
        statusCode: 404,
        message: errorMessage.USER_NOT_EXISTS,
      };
    }
    return UserRepository.update(id, dataToUpdate);
  }
  deleteOne(id) {
    if (!this.isExist(id)) {
      throw {
        statusCode: 404,
        message: errorMessage.USER_NOT_EXISTS,
      };
    }
    return UserRepository.delete(id);
  }
  // check if the entry exists
  isExist(id) {
    return !!UserRepository.getOne({ id });
  }
}

module.exports = new UserService();

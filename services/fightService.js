const { FightRepository } = require("../repositories/fightRepository");

class FightersService {
  // OPTIONAL TODO: Implement methods to work with fights
  getAll() {
    let fights = FightRepository.getAll();
    return fights;
  }

  createOne(fight) {
    return FightRepository.create(fight);
  }
  getOne(id) {
    if (!this.isExist(id)) {
      throw {
        statusCode: 404,
        message: errorMessage.USER_NOT_EXISTS,
      };
    }
    return FightRepository.getOne({ id });
  }
  deleteOne(id) {
    if (!this.isExist(id)) {
      throw {
        statusCode: 404,
        message: errorMessage.USER_NOT_EXISTS,
      };
    }
    return FightRepository.delete(id);
  }
  isExist(id) {
    return !!FightRepository.getOne({ id });
  }
}

module.exports = new FightersService();

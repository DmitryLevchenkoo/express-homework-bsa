const { FighterRepository } = require("../repositories/fighterRepository");
const { UserRepository } = require("../repositories/userRepository");
const errorMessage = require("../constants/error-messages");
class FighterService {
  // TODO: Implement methods to work with fighters
  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
  getAll() {
    const items = FighterRepository.getAll();
    return items;
  }
  getOne(id) {
    if (!this.isExist(id)) {
      throw {
        statusCode: 404,
        message: errorMessage.FIGHTER_NOT_EXISTS,
      };
    }
    return FighterRepository.getOne({ id });
  }
  addOne(data) {
    return FighterRepository.create(data);
  }
  updateOne(id, dataToUpdate) {
    if (!this.isExist(id)) {
      throw {
        statusCode: 404,
        message: errorMessage.FIGHTER_NOT_EXISTS,
      };
    }
    return FighterRepository.update(id, dataToUpdate);
  }
  deleteOne(id) {
    if (!this.isExist(id)) {
      throw {
        statusCode: 404,
        message: errorMessage.FIGHTER_NOT_EXISTS,
      };
    }
    return FighterRepository.delete(id);
  }
  // check if the entry exists
  isExist(id) {
    return !!FighterRepository.getOne({ id });
  }
}

module.exports = new FighterService();

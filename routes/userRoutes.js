const { Router } = require("express");
const UserService = require("../services/userService");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

// TODO: Implement route controllers for user
router.get("/", function (req, res, next) {
  res.data = UserService.getAll();
  next();
});
router.get("/:id", function (req, res, next) {
  res.data = UserService.getOne(req.params.id);
  next();
});
router.post("/", createUserValid, function (req, res, next) {
  res.data = UserService.addOne(req.body);
  next();
});
router.put("/:id", updateUserValid, function (req, res, next) {
  res.data = UserService.updateOne(req.params.id, req.body);
  next();
});
router.delete("/:id", function (req, res, next) {
  res.data = UserService.deleteOne(req.params.id);
  next();
});

router.use(responseMiddleware);

module.exports = router;

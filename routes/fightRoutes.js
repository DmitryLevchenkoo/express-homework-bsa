const { Router } = require("express");
const FightService = require("../services/fightService");
const {
  createFightValid,
} = require("../middlewares/fight.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

router.get("/", (req, res, next) => {
  res.data = FightService.getAll();
  next();
});
router.get("/:id", function (req, res, next) {
  console.log(req.params.id);
  res.data = FightService.getOne(req.params.id);
  next();
});
router.post("/", createFightValid, (req, res, next) => {
  res.data = FightService.createOne(req.body);
  next();
});
router.delete("/:id", function (req, res, next) {
  res.data = FightService.deleteOne(req.params.id);

  next();
});
router.use(responseMiddleware);

module.exports = router;

const { Router } = require("express");
const FighterService = require("../services/fighterService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createFighterValid,
  updateFighterValid,
} = require("../middlewares/fighter.validation.middleware");

const router = Router();

// TODO: Implement route controllers for fighter
router.get("/", function (req, res, next) {
  res.data = FighterService.getAll();

  next();
});

router.get("/:id", function (req, res, next) {
  res.data = FighterService.getOne(req.params.id);
  next();
});

router.post("/", createFighterValid, function (req, res, next) {
  res.data = FighterService.addOne(req.body);

  next();
});

router.put("/:id", updateFighterValid, function (req, res, next) {
  res.data = FighterService.updateOne(req.params.id, req.body);

  next();
});

router.delete("/:id", function (req, res, next) {
  res.data = FighterService.deleteOne(req.params.id);

  next();
});

router.use(responseMiddleware);

module.exports = router;

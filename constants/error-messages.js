const messages = {
  PROBLEM_ON_SERVER: "the error occured on the server",
  FIGHTER_NOT_EXISTS: "fighter with such id doesnt exists",
  USER_NOT_EXISTS: "user with such id doesnt exists",
  MUST_HAVE_AT_LEAST_ONE_PROPERTY:
    "update data must have at least one property",

  FIGHTERS_LIST_EMPTY: "cannot find any fighters",
  USERS_LIST_EMPTY: "cannot find any users",
  FIGHTS_LIST_EMPTY: "cannot find any fights",

  HAS_EXTRA_FIELDS: "entity has an extra fields",

  INVALID_EMAIL: "email is not valid",
  USER_WITH_SAME_EMAIL_EXISTS: "user with given email already exists",

  INVALID_PHONE_NUMBER: "phone number is not valid",
  USER_WITH_SAME_PHONE_NUMBER_EXISTS: "user with same phone already exists",
  FIGHTER_WITH_SAME_NAME_EXISTS: "fighter with same name already exists",

  INVALID_PASSWORD: "password is not valid",

  INVALID_NAME: "name is not valid",
  INVALID_LAST_NAME: "last name is not valid",
  SHOULD_NOT_CONTAIN_ID: "entity should not contain id property",

  INVALID_FIGHTER_NAME: "name is invalid",
  INVALID_HEALTH: "health property is invalid",
  INVALID_POWER: "power property is invalid",
  INVALID_DEFENCE: "defense property is invalid",

  FIGHT_LOG_EMPTY: "fight log if empty",
  INVALID_FIGHT_LOG: "fight log is invalid",
};

module.exports = Object.freeze(messages);

//The default error handler
module.exports = function (err, req, res, next) {
  if (res.headersSent) {
    next(err);
  } else {
    const status = err.status || 400;
    const message = err.message || "";
    res.status(status).json({ message, error: true });
  }
};

const { user } = require("../models/user");
const UserService = require("../services/userService");
const errorMessage = require("../constants/error-messages");
const {
  userCreateValidator: validateUserAdd,
  userUpdateValidator: validateUserUpdate,
} = require("../utils/validators/user.validator");
const createUserValid = (req, res, next) => {
  const error = validateUserAdd(req.body, next);
  if (error) {
    throw { message: error, status: 400 };
  }

  checkOnUniqueness(req.body.email, req.body.phoneNumber);

  next();
};

const updateUserValid = (req, res, next) => {
  if (Object.keys(req.body).length < 1) {
    throw {
      message: errorMessage.MUST_HAVE_AT_LEAST_ONE_PROPERTY,
      status: 400,
    };
  }
  const error = validateUserUpdate(req.body, next);
  if (error) {
    throw { message: error, status: 400 };
  }
  // TODO: Implement validatior for user entity during update

  checkOnUniqueness(
    req.body.email ? req.body.email : null,
    req.body.phoneNumber ? req.body.phoneNumber : null
  );
  next();
};

function checkOnUniqueness(email, phoneNumber) {
  if (email) {
    if (UserService.search({ email })) {
      throw { message: errorMessage.USER_WITH_SAME_EMAIL_EXISTS, status: 400 };
    }
  }
  if (phoneNumber) {
    if (UserService.search({ phoneNumber })) {
      throw {
        message: errorMessage.USER_WITH_SAME_PHONE_NUMBER_EXISTS,
        status: 400,
      };
    }
  }
}
exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;

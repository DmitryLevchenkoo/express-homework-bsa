const { fighter } = require("../models/fighter");
const errorMessage = require("../constants/error-messages");
const {
  fighterCreateValidator,
  fighterUpdateValidator,
} = require("../utils/validators/fighters.validator");
const FighterService = require("../services/fighterService");

const createFighterValid = (req, res, next) => {
  const error = fighterCreateValidator(req.body, next);
  if (error) {
    throw { message: error, status: 400 };
  }
  checkOnUniqueness(req.body.name);
  next();
};

const updateFighterValid = (req, res, next) => {
  const error = fighterUpdateValidator(req.body, next);
  if (error) {
    throw { message: error, status: 400 };
  }
  checkOnUniqueness(req.body.name ? req.body.name : null);

  next();
};
function checkOnUniqueness(name) {
  if (name) {
    if (FighterService.search({ name })) {
      throw {
        message: errorMessage.FIGHTER_WITH_SAME_NAME_EXISTS,
        status: 400,
      };
    }
  }
}
exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;

const { fighter } = require("../models/fighter");
const errorMessage = require("../constants/error-messages");
const fightValidator = require("../utils/validators/fights.validator");
const createFightValid = (req, res, next) => {
  const error = fightValidator(req.body, next);
  if (error) {
    throw { message: error, status: 400 };
  }
  next()
};

exports.createFightValid = createFightValid;

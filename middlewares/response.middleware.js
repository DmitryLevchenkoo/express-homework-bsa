const errorMessages = require("../constants/error-messages");

const responseMiddleware = (req, res, next) => {
  if (!res.data) {
    throw { message: errorMessages.PROBLEM_ON_SERVER, status: 400 };
  } else {
    res.status(200).send(res.data);
  }
  next();
};

exports.responseMiddleware = responseMiddleware;

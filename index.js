const express = require("express");
const cors = require("cors");
const errorHandler = require("./middlewares/error-handler.middlware");
require("./config/db");

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const routes = require("./routes/index");
routes(app);

app.use("/", express.static("./client/build"));


app.use(errorHandler);

const port = 3050;
app.listen(port, () => {
  console.log(`server start on port ${port} at http://localhost:${port}/`);
});

exports.app = app;
